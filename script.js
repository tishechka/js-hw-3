/*
1) Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.

Цикли в програмуванні важливі тим, що вони дозволяють виконувати повторювані дії без вписування одної і тої самої команди кілька разів.
Вони зменшують кількість коду, а отже роблять розробку більш ефективною та зручною.
Цикли дозволяють також обробляти дані, що зберігаються у структурах даних, таких як масиви або списки.

2) Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.

    1. for: використовую, коли вже відома кількість повторень циклу.
    2. while: використовую, коли кількість повторень циклу не відома наперед.
Виконання блоку коду буде продовжуватися до тих пір, поки певна умова залишається істинною.

3) Що таке явне та неявне приведення (перетворення) типів даних у JS?

Явне перетворення типів: відбувається, коли ми свідомо змінюємо тип даних одного значення на інший. Це можна зробити за допомогою функцій Number(), String(), Boolean(), або операторів як parseInt(), parseFloat()

Неявне приведення типів: відбувається автоматично. Наприклад, коли я виконую операторів або операції між різними типами даних, JavaScript автоматично виконує неявне приведення типів для забезпечення відповідного результату.
*/


function isInteger(value) {
    return Number.isInteger(parseFloat(value));
}

function getUserNumber() {
    let userInput;
    do {
        userInput = prompt("Введіть число:");
    } while (!isInteger(userInput));

    return parseInt(userInput);
}

const userNumber = getUserNumber();

const numbers = [];
for (let i = 0; i <= userNumber; i++) {
    if (i % 5 === 0) {
        numbers.push(i);
    }
}

if (numbers.length > 0) {
    console.log("Числа, кратні 5:", numbers);
} else {
    console.log("Sorry, no numbers");
}

function isPrime(number) {
    if (number <= 1) {
        return false;
    }

    for (let i = 2; i <= Math.sqrt(number); i++) {
        if (number % i === 0) {
            return false;
        }
    }

    return true;
}

function getUserNumbers() {
    let m, n;
    do {
        m = getUserNumber();
        n = getUserNumber();
        if (!isInteger(m) || !isInteger(n)) {
            console.log("Помилка! Введені значення повинні бути цілими числами.");
        } else if (m >= n) {
            console.log("Помилка! Перше число повинно бути менше за друге.");
        }
    } while (!isInteger(m) || !isInteger(n) || m >= n);

    return { m, n };
}

const { m, n } = getUserNumbers();

const primes = [];
for (let i = m; i <= n; i++) {
    if (isPrime(i)) {
        primes.push(i);
    }
}

if (primes.length > 0) {
    console.log("Прості числа:", primes);
} else {
    console.log("Sorry, no prime numbers");
}

